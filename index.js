const products = [
    {
        title:"Instant Pot",
        popularity:98,
        price:59
    },
    {
        title:"Blue Light Blocking Glasses",
        popularity:90,
        price:16
    },
    {
        title:"Fire TV Stick",
        popularity:48,
        price:49
    },
    {
        title:"Wyze Cam",
        popularity:48,
        price:25
    },
    {
        title:"Water Filter",
        popularity:56,
        price:49
    },    
    {
        title:"Iphone Case",
        popularity:90,
        price:15
    },    
    {
        title:"Ice Maker",
        popularity:47,
        price:119
    },    
    {
        title:"Video Doorbell",
        popularity:47,
        price:199
    },    
    {
        title:"AA Batteries",
        popularity:64,
        price:12
    },    
    {
        title:"Disinfecting Wipes",
        popularity:37,
        price:12
    },    
    {
        title:"Baseball Cards",
        popularity:73,
        price:16
    },    
    {
        title:"Winter Gloves",
        popularity:32,
        price:112
    },    
    {
        title:"Microphone",
        popularity:44,
        price:22
    },    
    {
        title:"Pet Kennel",
        popularity:5,
        price:24
    },    
    {
        title:"Jenga Classic Game",
        popularity:100,
        price:7
    },    
    {
        title:"Ink Cartridg",
        popularity:88,
        price:45
    },    
    {
        title:"Selfie Stick",
        popularity:98,
        price:29
    },    
    {
        title:"Hoze Nozzle",
        popularity:74,
        price:26
    },    
    {
        title:"Gift Card",
        popularity:45,
        price:25
    },    
    {
        title:"Keyboard",
        popularity:82,
        price:19
    }
    
];

const sortArrayObject  = (data) => {
    let sortedArrayObject = [...data];
    sortedArrayObject.sort((x,y) => {
        if (x.popularity > y.popularity){ // Sort by mosr popular first
            return -1;
        }
        else if (x.popularity == y.popularity){
            if (x.price > y.price) // if products are equally popular, sort by cheapest price (lower is better)
                return 1;
            else return -1;
        }
        else {
            return 1;
        }
    });
    return sortedArrayObject;
}


let sortedProducts  = sortArrayObject(products);
console.log("Sorted Product by most popular and price:")
for (let i = 0; i < sortedProducts.length; i++)
    console.log(sortedProducts[i].title + ", " + sortedProducts[i].popularity + ", " + sortedProducts[i].price);

